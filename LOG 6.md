NAME: SYED ASIF UZ ZAMAN

MATRIC : 191587


**Went to propulsion lab for thrust testing**

- Test 2nd motor with power supply (16V) and also battery (15.8V) with ESC 
- Wrong propeller orientation causing lots of vibration and unstable thrust.

   **Solution:** choose the correct propeller with correct orientation.

-  Error in troubleshooting current problem due to poor connection.

- After all troubleshoot, we were able to obtain thrust of 1200+- gf of thrust when given 100% of throttle with the battery (15.8V) and 17inch propeller.

**Thrust Results**

T-Motor KV340 (x4) = 249g (x4) = **996g **

T-Motor ALPHA 40A 6S ESC (x4) = 54.5g (x4) = **218g**

TrackStar TS-411MG Digital 1/10 Scale Short Course Steering Servo 25T (x4) = 57g (x4) = **228g**

Mass of propulsion system (without thruster arms) = **1442g** (maybe give a clearance and round off to **1500g **)


**Did Theorotical Thrust and power calculation.**

[calculation](https://gitlab.com/191587/log-book/-/blob/main/Screenshot_2021-11-26_150636.png)



