**NAME:** SYED ASIF UZ ZAMAN

**MATRIC:** 191587

Went to the propulsion lab to test out old motors with similar specifications [Tarot 4114/320KV Brushless Motor](http://www.tarotrc.com/Product/Detail.aspx?Lang=en&Id=7caf622a-6119-4aaf-90e2-ed846acfb13f)

- out of 4 , 2 motors were functioning,

- One motor was in good condition and the max thrust and current were T = 1.00kg, I = 6.13A

   gathered data for the motor:

   [current vs time](https://gitlab.com/putraspace/idp-2021-group/propulsion-and-power/-/raw/main/Images_&_Videos/motor_2_current.png)


   [Thrust vs Time](https://gitlab.com/putraspace/idp-2021-group/propulsion-and-power/-/raw/main/Images_&_Videos/motor_2_thrust.png)




- Another motor was functioning but had vibration, the thrust and current measured were respectively T = 0.49kg, I = 12.70A

[current vs time](https://gitlab.com/putraspace/idp-2021-group/propulsion-and-power/-/raw/main/Images_&_Videos/motor_3_current.png)


[Thrust vs Time](https://gitlab.com/putraspace/idp-2021-group/propulsion-and-power/-/raw/main/Images_&_Videos/motor_3_thrust.png)


- All motors were tested using 17 inch propeller.


-  Dr. Ezanee gave a brief explanantion on how we should be able to find the thrust that is required from the equilibrium of forces of the airship. 

