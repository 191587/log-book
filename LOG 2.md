**NAME:** SYED ASIF UZ ZAMAN

**MATRIC:** 191587

**DATE:** 11 November 2021

**AGENDA:** perform the thrust test of the motor to figure out the thrust required for the airship as we need it to desing the propulsion and power system of our airship for the project.

**Goals:** To find out the right testing method to perform the thrust test accurately and to determine the thrust, accelaration,RPM, vibration etc parameters of the motor.

**Method:** 
- We went to propulsion lab to examine a motor similar to the one which is gonna be used by us in the project.


- Few testing was done to determine the most efficient way to perform the thrust test.


- the trolly method was chosen to perform one initial thrust test.


- A meeting was held with Dr Ezanee regarding the matter.

**issues regarding the testing:** 

- For the thrust test, its very important for us to know the structure weight, sprayer weight etc about which we still dont have much information so we have to rely on our estimation.

**Next step:** 

- Currently using theorotical method to calculate the thrust by manual calculation.

- Planning to perform the thrust test using the trolly method with weight estimation.

